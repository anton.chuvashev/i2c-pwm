# I2c PWM


## Getting started

Project contains two parts:

Master, written in Micropython, which can send JSON commands to adjust PWM levels on...

Slave, written in C++ which tunes 16 PWM pins with values read from JSON

## Hardware

Master can be run on any Micropython board, for example ESP12, ESP32, RP2, etc.

Slave adjusted for RP2040 boards, but it is sutable for almost any microcontroller

## Python example

```
from i2c_pwm import I2CPwm

ic = I2CPwm()

ic.set_pin(1,10000) # set pin 1 to value 10000
ic.stop_pin(9) # set pin 9 to 0
ic.stop_all() # set all pint to 0
```
