// Class to be used with board like Waveshare RP2040 zero
// Initiates I2c slave on pins 26, 27
// Uses I2c address 8 by default
// Uses pins 0..15 as PWM out in range 0..65535
// To control PWMs send JSON data via I2c
// Example: {"9":12345} set value 12345 on pin 9
// PWM Frequency on RP2040 is 2 kHz
// In case of JSON error a Red led lights on board
// In all cases of ok operations a Green led lights on board
// While board processing JSON a Yellow led lights on board

#include <Wire.h>
#include "pico/stdlib.h"
#include "hardware/pwm.h"
#include <ArduinoJson.h>
#include <Adafruit_NeoPixel.h>

#define PWM_PIN0 0
#define PWM_PIN1 1
#define PWM_PIN2 2
#define PWM_PIN3 3
#define PWM_PIN4 4
#define PWM_PIN5 5
#define PWM_PIN6 6
#define PWM_PIN7 7
#define PWM_PIN8 8
#define PWM_PIN9 9
#define PWM_PIN10 10
#define PWM_PIN11 11
#define PWM_PIN12 12
#define PWM_PIN13 13
#define PWM_PIN14 14
#define PWM_PIN15 15

#define PIN_ZERO 28

#define JSON_BUFFER_SIZE 512

#define LED_PIN 16

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(1, LED_PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  gpio_init(PIN_ZERO);
  gpio_set_dir(PIN_ZERO, GPIO_OUT);
  gpio_put(PIN_ZERO, 0);

  gpio_set_function(PWM_PIN0, GPIO_FUNC_PWM);
  gpio_set_function(PWM_PIN1, GPIO_FUNC_PWM);
  gpio_set_function(PWM_PIN2, GPIO_FUNC_PWM);
  gpio_set_function(PWM_PIN3, GPIO_FUNC_PWM);
  gpio_set_function(PWM_PIN4, GPIO_FUNC_PWM);
  gpio_set_function(PWM_PIN5, GPIO_FUNC_PWM);
  gpio_set_function(PWM_PIN6, GPIO_FUNC_PWM);
  gpio_set_function(PWM_PIN7, GPIO_FUNC_PWM);
  gpio_set_function(PWM_PIN8, GPIO_FUNC_PWM);
  gpio_set_function(PWM_PIN9, GPIO_FUNC_PWM);
  gpio_set_function(PWM_PIN10, GPIO_FUNC_PWM);
  gpio_set_function(PWM_PIN11, GPIO_FUNC_PWM);
  gpio_set_function(PWM_PIN12, GPIO_FUNC_PWM);
  gpio_set_function(PWM_PIN13, GPIO_FUNC_PWM);
  gpio_set_function(PWM_PIN14, GPIO_FUNC_PWM);
  gpio_set_function(PWM_PIN15, GPIO_FUNC_PWM);

  pwm_set_wrap(pwm_gpio_to_slice_num(PWM_PIN0), 65535);
  pwm_set_wrap(pwm_gpio_to_slice_num(PWM_PIN2), 65535);
  pwm_set_wrap(pwm_gpio_to_slice_num(PWM_PIN4), 65535);
  pwm_set_wrap(pwm_gpio_to_slice_num(PWM_PIN6), 65535);
  pwm_set_wrap(pwm_gpio_to_slice_num(PWM_PIN8), 65535);
  pwm_set_wrap(pwm_gpio_to_slice_num(PWM_PIN10), 65535);
  pwm_set_wrap(pwm_gpio_to_slice_num(PWM_PIN12), 65535);
  pwm_set_wrap(pwm_gpio_to_slice_num(PWM_PIN14), 65535);

  stopAll();

  pwm_set_enabled(pwm_gpio_to_slice_num(PWM_PIN0), true);
  pwm_set_enabled(pwm_gpio_to_slice_num(PWM_PIN2), true);
  pwm_set_enabled(pwm_gpio_to_slice_num(PWM_PIN4), true);
  pwm_set_enabled(pwm_gpio_to_slice_num(PWM_PIN6), true);
  pwm_set_enabled(pwm_gpio_to_slice_num(PWM_PIN8), true);
  pwm_set_enabled(pwm_gpio_to_slice_num(PWM_PIN10), true);
  pwm_set_enabled(pwm_gpio_to_slice_num(PWM_PIN12), true);
  pwm_set_enabled(pwm_gpio_to_slice_num(PWM_PIN14), true);

  Wire1.setSCL(27);
  Wire1.setSDA(26);

  Wire1.begin(8);
  Wire1.onReceive(receiveEvent);

  pixels.begin();
  showGreen();
}

void loop() {
  delay(100);
}

void receiveEvent(int numBytes) {
  showYellow();

  char buffer[JSON_BUFFER_SIZE];
  
  for (int i = 0; i < numBytes; i++) {
    buffer[i] = Wire1.read();
  }

  JsonDocument json_doc;

  DeserializationError error = deserializeJson(json_doc, buffer);

  if (error) {
    stopAll();
    showRed();
  }
  else {
    pwm_set_gpio_level(PWM_PIN0, json_doc["0"]);
    pwm_set_gpio_level(PWM_PIN1, json_doc["1"]);
    pwm_set_gpio_level(PWM_PIN2, json_doc["2"]);
    pwm_set_gpio_level(PWM_PIN3, json_doc["3"]);
    pwm_set_gpio_level(PWM_PIN4, json_doc["4"]);
    pwm_set_gpio_level(PWM_PIN5, json_doc["5"]);
    pwm_set_gpio_level(PWM_PIN6, json_doc["6"]);
    pwm_set_gpio_level(PWM_PIN7, json_doc["7"]);
    pwm_set_gpio_level(PWM_PIN8, json_doc["8"]);
    pwm_set_gpio_level(PWM_PIN9, json_doc["9"]);
    pwm_set_gpio_level(PWM_PIN10, json_doc["10"]);
    pwm_set_gpio_level(PWM_PIN11, json_doc["11"]);
    pwm_set_gpio_level(PWM_PIN12, json_doc["12"]);
    pwm_set_gpio_level(PWM_PIN13, json_doc["13"]);
    pwm_set_gpio_level(PWM_PIN14, json_doc["14"]);
    pwm_set_gpio_level(PWM_PIN15, json_doc["15"]);

    showGreen();
  }
}

void stopAll() {
    pwm_set_gpio_level(PWM_PIN0, 0);
    pwm_set_gpio_level(PWM_PIN1, 0);
    pwm_set_gpio_level(PWM_PIN2, 0);
    pwm_set_gpio_level(PWM_PIN3, 0);
    pwm_set_gpio_level(PWM_PIN4, 0);
    pwm_set_gpio_level(PWM_PIN5, 0);
    pwm_set_gpio_level(PWM_PIN6, 0);
    pwm_set_gpio_level(PWM_PIN7, 0);
    pwm_set_gpio_level(PWM_PIN8, 0);
    pwm_set_gpio_level(PWM_PIN9, 0);
    pwm_set_gpio_level(PWM_PIN10, 0);
    pwm_set_gpio_level(PWM_PIN11, 0);
    pwm_set_gpio_level(PWM_PIN12, 0);
    pwm_set_gpio_level(PWM_PIN13, 0);
    pwm_set_gpio_level(PWM_PIN14, 0);
    pwm_set_gpio_level(PWM_PIN15, 0);
}

void showRed() {
  pixels.setPixelColor(0, pixels.Color(0,20,0));
  pixels.show();
}

void showYellow() {
  pixels.setPixelColor(0, pixels.Color(10,10,0));
  pixels.show();
}

void showGreen() {
  pixels.setPixelColor(0, pixels.Color(20,0,0));
  pixels.show();
}
