from machine import I2C
import json
from time import sleep_ms

class I2CPwm():
    """
        Class for communication with i2c pwm device
        Usage:
            ic = I2CPwm()

            ic.set_pin(pin,value)
            ic.stop_all()
            ic.stop_pin(pin)

            pin   - pin number from 0 to 16
            value - 0..65535
    """
    def __init__(self, addr=8, scl=5, sda=4, timeout=1000, freq=400000, pin_count=16):
        self.i2c = I2C(freq=freq, scl=scl, sda=sda, timeout=timeout)
        self.pins = {}
        self.addr = addr
        self.pin_count = pin_count
        self.stop_all()

    def stop_all(self):
        self.pins = dict(zip([str(i) for i in range(self.pin_count)], [0] * self.pin_count))
        self._apply()

    def stop_pin(self, pin):
        self.pins[pin] = 0
        self._apply()

    def set_pin(self, pin, value):
        self.pins[pin] = value
        self._apply()

    def _apply(self):
        self.i2c.writeto(self.addr, self._serialize(self.pins))

    def _serialize(self, pin_data):
        return bytes(json.dumps(pin_data, separators=(',', ':')), 'UTF-8')
